<?php

namespace Koncewicz\NewPrice\Block\Cart;

class Alert extends \Magento\Checkout\Block\Cart\AbstractCart
{
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Checkout\Model\Session $checkoutSession,
        array $data = []
    ) {
        parent::__construct($context, $customerSession, $checkoutSession, $data);
    }
}
